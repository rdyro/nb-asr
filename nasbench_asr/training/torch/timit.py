import pathlib

import torch
import torchaudio
import torchvision
import numpy as np

from .encoder import PhonemeEncoder


torchaudio.set_audio_backend("sox_io")


class TimitDataset(torch.utils.data.Dataset):
    def __init__(self, root_folder, encoder, subset="TRAIN", ignore_sa=True, transforms=None):
        root = pathlib.Path(root_folder).expanduser()
        wavs = list(root.rglob(f"{subset}/**/*.RIFF.WAV"))
        wavs = sorted(wavs)
        if ignore_sa:
            wavs = [w for w in wavs if not w.name.startswith("SA")]
        phonemes = [(f.parent / f.stem).with_suffix(".PHN") for f in wavs]

        self.audio = []
        self.audio_len = []
        for wav in wavs:
            tensor, sample_rate = torchaudio.load(str(wav))
            self.audio.append(tensor)
            self.audio_len.append(tensor.shape[1] / sample_rate)

        def load_sentence(f):
            lines = f.read_text().strip().split("\n")
            last = [l.rsplit(" ", maxsplit=1)[-1] for l in lines]
            last = encoder.encode(last)
            return last

        self.root_folder = root_folder
        self.encoder = encoder
        self.sentences = [load_sentence(f) for f in phonemes]
        self.transforms = transforms

        assert len(self.audio) == len(self.sentences)

    def __len__(self):
        return len(self.audio)

    def __getitem__(self, idx):
        audio = self.audio[idx]
        sentence = self.sentences[idx]
        if self.transforms is not None:
            audio = self.transforms(audio)
        return audio, sentence

    def get_indices_shorter_than(self, time_limit):
        return [
            i
            for i, audio_len in enumerate(self.audio_len)
            if time_limit is None or audio_len < time_limit
        ]


def pad_sequence_bft(sequences, extra=0, padding_value=0.0):
    batch_size = len(sequences)
    leading_dims = sequences[0].shape[:-1]
    max_t = max([s.shape[-1] + extra for s in sequences])

    out_dims = (batch_size,) + leading_dims + (max_t,)

    out_tensor = sequences[0].new_full(out_dims, padding_value)
    for i, tensor in enumerate(sequences):
        length = tensor.shape[-1]
        out_tensor[i, ..., :length] = tensor

    return out_tensor


def pad_sentences(sequences, padding_value=0.0):
    max_t = max([len(s) for s in sequences])
    sequences = [s + [0] * (max_t - len(s)) for s in sequences]
    return sequences


def normalize_fn(audio, eps=0.001):
    moving_mean = np.array(
        [
            [-11.184436, -9.882508, -8.594713, -8.681572, -7.2675357],
            [-7.5756226, -7.4582334, -6.506399, -6.558333, -6.420634],
            [-6.638953, -6.691627, -6.498169, -6.1360364, -5.939979],
            [-5.8920403, -5.967334, -6.1402273, -6.324319, -6.386209],
            [-6.4368715, -6.5349183, -6.6634316, -6.8013744, -6.942908],
            [-7.0817432, -7.1181254, -7.4412394, -7.5745416, -7.466323],
            [-7.8425217, -7.692717, -7.861338, -7.7798257, -7.896208],
            [-7.80984, -7.8167634, -7.806974, -7.813973, -7.893374],
            [-7.9918337, -8.098805, -8.167498, -8.2140045, -8.244688],
            [-8.257044, -8.179695, -8.242458, -8.19206, -8.301322],
            [-8.360076, -8.530276, -8.619728, -8.66062, -8.621213],
            [-8.521931, -8.42586, -8.332798, -8.338442, -8.426057],
            [-8.572788, -8.762552, -8.994527, -9.1751375, -9.344926],
            [-9.552911, -9.784323, -10.003114, -10.141138, -10.188528],
            [-10.229798, -10.225428, -10.199504, -10.175425, -10.11461],
            [-10.143187, -10.198475, -10.219165, -10.185537, -10.104565],
        ]
    ).reshape(-1)
    moving_variance = np.array(
        [
            [3.4309013, 3.4321866, 9.846313, 9.84611, 12.289771],
            [12.796532, 13.610857, 16.238516, 16.523462, 16.441185],
            [16.75778, 16.76997, 17.777157, 19.134811, 20.280005],
            [20.767492, 20.749187, 20.844742, 20.18181, 19.463705],
            [19.437, 18.743654, 17.7808, 16.79943, 15.816132],
            [15.199439, 14.797875, 14.643991, 14.683314, 14.56193],
            [14.619057, 14.183195, 14.128502, 13.946552, 13.952582],
            [13.815644, 13.654697, 13.413378, 13.300857, 13.144859],
            [12.85179, 12.494914, 12.085991, 11.797006, 11.545486],
            [11.509701, 11.593503, 11.870104, 11.922072, 11.775043],
            [11.358956, 10.967069, 10.720085, 10.601389, 10.730139],
            [11.106731, 11.475688, 11.699212, 11.549972, 11.265011],
            [10.905409, 10.495268, 10.200081, 10.099484, 9.981604],
            [9.78471, 9.4420805, 9.082503, 8.86038, 8.625089],
            [8.3983555, 8.129456, 8.004622, 7.85795, 7.6699963],
            [7.5044293, 7.3248878, 7.225893, 7.1065283, 7.055343],
        ]
    ).reshape(-1)
    mean = moving_mean[None, :, None]
    variance = moving_variance[None, :, None]
    return (audio - mean) / (variance + eps)


def get_normalize_fn(part_name, eps=0.001):
    stats = np.load(pathlib.Path(__file__).parents[1].joinpath(f"timit_train_stats.npz"))
    mean = stats["moving_mean"][None, :, None]
    variance = stats["moving_variance"][None, :, None]

    def normalize(audio):
        return (audio - mean) / (variance + eps)

    return normalize


def collate_fn(batch):
    audio = [b[0][0] for b in batch]
    audio_lengths = [a.shape[-1] for a in audio]
    sentence = [b[1] for b in batch]
    sentence_lengths = [len(s) for s in sentence]
    audio = pad_sequence_bft(audio, extra=0, padding_value=0.0)
    sentence = pad_sentences(sentence, padding_value=0.0)
    return (audio, torch.tensor(audio_lengths)), (
        torch.tensor(sentence, dtype=torch.int32),
        torch.tensor(sentence_lengths),
    )


def get_dataloaders(timit_root, batch_size):
    encoder = PhonemeEncoder(48)

    # def get_transforms(part_name):
    #    transforms = torchvision.transforms.Compose(
    #        [
    #            torchaudio.transforms.MelSpectrogram(
    #                sample_rate=16000, win_length=400, hop_length=160, n_mels=80
    #            ),
    #            torch.log,
    #            get_normalize_fn(part_name),
    #        ]
    #    )
    #    return transforms

    transforms = torchvision.transforms.Compose(
        [
            torchaudio.transforms.MelSpectrogram(
                sample_rate=16000, win_length=400, hop_length=160, n_mels=80
            ),
            torch.log,
            normalize_fn,
        ]
    )

    subsets = ["TRAIN", "VAL", "TEST"]
    datasets = [
        # TimitDataset(timit_root, encoder, subset=s, ignore_sa=True, transforms=get_transforms(s))
        TimitDataset(timit_root, encoder, subset=s, ignore_sa=True, transforms=transforms)
        for s in subsets
    ]
    train_sampler = torch.utils.data.SubsetRandomSampler(datasets[0].get_indices_shorter_than(None))
    loaders = [
        torch.utils.data.DataLoader(
            d,
            batch_size=batch_size,
            sampler=train_sampler if not i else None,
            pin_memory=True,
            collate_fn=collate_fn,
        )
        for i, d in enumerate(datasets)
    ]
    return (encoder, *loaders)


def set_time_limit(loader, time_limit):
    db = loader.dataset
    sampler = loader.sampler
    sampler.indices = db.get_indices_shorter_than(time_limit)


if __name__ == "__main__":
    import pprint

    train_load, val_load, test_load = get_dataloaders("TIMIT", 3)
    for (audio, lengths), sentence in train_load:
        print(audio.shape, audio)
        print()
        print(lengths.shape, lengths)
        print()
        print(sentence)
        break
